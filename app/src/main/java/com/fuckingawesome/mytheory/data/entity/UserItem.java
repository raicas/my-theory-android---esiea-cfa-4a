package com.fuckingawesome.mytheory.data.entity;

/**
 * Created by allan on 13/03/2018.
 */

public class UserItem {

    private String idUser;
    private String userName;
    private String mail;

    public UserItem() {

    }

    public UserItem(String idUser, String userName, String mail) {
        this.idUser = idUser;
        this.userName = userName;
        this.mail = mail;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
