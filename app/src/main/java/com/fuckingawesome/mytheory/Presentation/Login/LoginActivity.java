package com.fuckingawesome.mytheory.Presentation.Login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.fuckingawesome.mytheory.Presentation.MainActivity;
import com.fuckingawesome.mytheory.R;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class LoginActivity extends AppCompatActivity implements LoginHandler.LoginListener, LoginHandler.SendPasswordListener {



    LoginChoiceFragment loginChoiceFragment = new LoginChoiceFragment();
    CreateAccountFragment createAccountFragment = new CreateAccountFragment();
    public static LoginHandler loginHandler = LoginHandler.instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        replaceFragment(loginChoiceFragment, false);
        loginHandler.setLoginListener(this);
    }

    public void onInviteButtonClick(View view) {
        LoginHandler.instance.setInviteMode(true);
        launchMainActivity();

    }

    public void replaceFragment(Fragment fragment, Boolean isAddingToStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.login_fragment_container, fragment);
        if (isAddingToStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void launchMainActivity() {
        Intent myIntent = new Intent(LoginActivity.this, MainActivity.class);
        LoginActivity.this.startActivity(myIntent);
        finish();

    }

    public static void launchLoginActivity(Activity act) {
        Intent myIntent = new Intent(act, LoginActivity.class);
        act.startActivity(myIntent);
    }

    public void onPasswordForgotClick(View view) {

        String mail = loginChoiceFragment.getMailString();

        if (mail.isEmpty())
            Toast.makeText(this, R.string.error_empty_field, Toast.LENGTH_LONG).show();
        else if (LoginHandler.isEmailValid(mail))
            LoginHandler.instance.mdpOublier(mail, this);
        else
            Toast.makeText(this, R.string.error_invalid_mail, Toast.LENGTH_LONG).show();

    }

    /***
     * clic sur le boutton permettant d'accéder au fragment de création de compte
     * @param view
     */
    public void onCreateAccountClick(View view) {

        replaceFragment(createAccountFragment, true);

    }

    public void onLoginClick(View view){
        if (loginChoiceFragment.isConnectable())
            loginHandler.connexion(this, loginChoiceFragment.getMailString(), loginChoiceFragment.getPasswordString());

    }

    /***
     * clic sur le boutton permattant de créer son compte
     * @param view
     */
    public void onCreateAccountButtonClicked(View view) {

        createAccountFragment.createAccount();

    }



    @Override
    public void createAccountSuccesful() {

        onBackPressed();
    }

    @Override
    public void createAccountFailed(Exception e) {
        Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void connexionSuccesful() {

        launchMainActivity();
    }

    @Override
    public void connexionFailed(Exception e) {
        Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOperationComplete(boolean isSuccessful) {
        if (isSuccessful)
            Toast.makeText(this, R.string.forgotten_password_mail_send, Toast.LENGTH_LONG).show();

        else
            Toast.makeText(this, R.string.error_mdp_forgotten, Toast.LENGTH_LONG).show();


    }
}
