package com.fuckingawesome.mytheory.Presentation.Home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fuckingawesome.mytheory.GlideUtil;
import com.fuckingawesome.mytheory.Presentation.TheoryDetailActivity;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by allan on 02/04/2018.
 */

public class TheoryListAdapter extends RecyclerView.Adapter<TheoryListAdapter.ViewHolder> {

    private List<TheoryItem> theories = new ArrayList<>();


    // Provide a suitable constructor (depends on the kind of dataset)
    public TheoryListAdapter() {
        this.theories = theories;
    }

    @Override
    public TheoryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.theory_item, parent, false);

        final ViewHolder holder = new ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int itemPosition = holder.getAdapterPosition();
                TheoryItem item = theories.get(itemPosition);
                Context ctx = holder.couvertureImageItemTheory.getContext();
                String data = new Gson().toJson(item);
                Intent intent = new Intent(ctx, TheoryDetailActivity.class);
                intent.putExtra(TheoryDetailActivity.THEORY_ITEM, data);

                Pair<View, String> p1 = Pair.create((View) holder.couvertureImageItemTheory, "couvertureImageItemTheoryTransition");
                Pair<View, String> p2 = Pair.create((View) holder.titreTheoryTextView, "titreTextViewItemTheoryTransition");

                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) ctx, p1, p2);

                ctx.startActivity(intent, options.toBundle());
            }
        });
        return holder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        TheoryItem itm = theories.get(position);
        holder.titreTheoryTextView.setText(itm.getTitle());
        if (itm.getCouvertureImage() != null)
            Glide.with(holder.couvertureImageItemTheory.getContext()).load(itm.getCouvertureImage()).apply(GlideUtil.options).into(holder.couvertureImageItemTheory);

    }

    @Override
    public int getItemCount() {
        Log.d("dsfsfsf", Integer.toString(theories.size()));
        return theories.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView couvertureImageItemTheory;
        public TextView titreTheoryTextView;

        public ViewHolder(View v) {
            super(v);
            titreTheoryTextView = v.findViewById(R.id.titreTextViewItemTheory);
            couvertureImageItemTheory = v.findViewById(R.id.couvertureImageItemTheory);

        }
    }

    public void refreshList(List<TheoryItem> theories) {
        this.theories.clear();
        this.theories.addAll(theories);
        notifyDataSetChanged();
    }

    public void reverseList() {
        Collections.reverse(theories);
        notifyDataSetChanged();
    }

}
