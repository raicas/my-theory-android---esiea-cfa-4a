package com.fuckingawesome.mytheory;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.fuckingawesome.mytheory.Presentation.LibraryUsed.LibraryFragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;

public class LibraryService extends IntentService {

    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_GET_LIBRARY = "com.fuckingawesome.mytheory.action.GET_LIBRARY";
    public static final String LIBRARY_FILE = " librariess.json";
    private static final String LIBRARY_LINK = "https://pastebin.com/";


    public LibraryService() {
        super("LibraryService");
    }

    public static void startActionGET_LIBRARY(Context context) {
        Intent intent = new Intent(context, LibraryService.class);
        intent.setAction(ACTION_GET_LIBRARY);
        context.startService(intent);

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GET_LIBRARY.equals(action)) {
                handleActionGET_LIBRARY();
            }
        }
    }

    private void handleActionGET_LIBRARY() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(LIBRARY_LINK)
                .build();

        LibraryAPIService service = retrofit.create(LibraryAPIService.class);


        Call<ResponseBody> libraries = service.listLibraries();

        libraries.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String body = response.body().string();

                    File file = new File(getCacheDir(), LIBRARY_FILE);
                    writeToFile(body, getApplicationContext(), file);

                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(new Intent(LibraryFragment.LIBRARY_UPDATE));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });



    }

    private void writeToFile(String data, Context context, File file) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

        } catch (IOException e) {
            Log.e("hhh", "File write failed: " + e.toString());
        }
    }

    public interface LibraryAPIService {
        @GET("/raw/tXasR7ZJ/")
        Call<ResponseBody> listLibraries();
    }
}
