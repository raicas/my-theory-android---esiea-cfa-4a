package com.fuckingawesome.mytheory.Presentation.Login;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.fuckingawesome.mytheory.R;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LoginChoiceFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LoginChoiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LoginChoiceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private EditText mailEditText;
    private EditText passwordEditText;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LoginChoiceFragment() {
        // Required empty public constructor


    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LoginChoiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LoginChoiceFragment newInstance(String param1, String param2) {
        LoginChoiceFragment fragment = new LoginChoiceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login_choice, container, false);

        mailEditText = view.findViewById(R.id.mailLoginEditText);
        passwordEditText = view.findViewById(R.id.passwordLoginEditText);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //   throw new RuntimeException(context.toString()
            //        + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public boolean checkMailError () {

      /*  if (validEmail(this, findViewById(R.id.Email)) == true) {
            // TODO : function valid
        }*/
        return false;
    }

    public boolean isConnectable() {
        String mail = mailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        if (!LoginHandler.isEmailValid(mail)) {
            Toast.makeText(getContext(), R.string.error_invalid_mail, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.length() < LoginHandler.MIN_LENGHT_PASSWORD) {
            Toast.makeText(getContext(), R.string.error_password_too_short, Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public String getMailString() {
        return mailEditText.getText().toString().trim();
    }

    public String getPasswordString() {
        return passwordEditText.getText().toString();
    }

    public void clearEditText() {
        mailEditText.getText().clear();
        passwordEditText.getText().clear();
    }
}
