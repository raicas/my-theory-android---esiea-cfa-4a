package com.fuckingawesome.mytheory.Presentation.Login;

import android.app.Activity;
import android.util.Log;
import android.util.Patterns;

import com.fuckingawesome.mytheory.data.FirebaseQueries;
import com.fuckingawesome.mytheory.data.entity.UserItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Pattern;

import androidx.annotation.NonNull;


/**
 * Created by allan on 13/03/2018.
 */

public class LoginHandler {


    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static final String TAG_CONNEXION_FAILED = "TAG_CONNEXION";
    private static final String TAG_CREATE_ACCOUNT_FAILED = "TAG_CONNEXION";
    private LoginListener loginListener = null;
    private boolean isInviteMode = true;
    public static final LoginHandler instance = new LoginHandler();
    public static final int MIN_LENGHT_PASSWORD = 6;
    private UserItem user;

    private FirebaseUser currentFirebaseUser;

    private LoginHandler() {

        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                if (currentFirebaseUser != null)
                    getConnectedUserInfos(currentFirebaseUser.getUid());

            }
        });

    }

    public void setLoginListener(LoginListener loginListener) {
        this.loginListener = loginListener;
        if (currentFirebaseUser != null)
            getConnectedUserInfos(currentFirebaseUser.getUid());


    }

    public void createAccount(Activity act, final String username, final String email, String password) {

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(act, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (loginListener != null) {
                                setNewUserInfos(user.getUid(), username, email);

                                loginListener.createAccountSuccesful();
                            }


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG_CONNEXION_FAILED, "createUserWithEmail:failure", task.getException());
                            if (loginListener != null)
                                loginListener.createAccountFailed(task.getException());
                        }

                        // ...
                    }
                });
    }

    public void connexion(Activity act, String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(act, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            FirebaseUser user = mAuth.getCurrentUser();
                            isInviteMode = false;
                            getConnectedUserInfos(user.getUid());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG_CREATE_ACCOUNT_FAILED, "signInWithEmail:failure", task.getException());

                            if (loginListener != null)
                                loginListener.connexionFailed(task.getException());
                        }

                        // ...
                    }
                });

    }

    public void setNewUserInfos(String id, String username, String mail) {
        DatabaseReference dbr = FirebaseQueries.usersRef.child(id);
        UserItem user = new UserItem(id, username, mail);
        dbr.setValue(user);
    }

    public void getConnectedUserInfos(String id) {
        FirebaseQueries.usersRef.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                user = dataSnapshot.getValue(UserItem.class);
                Log.d("sfss", "klkml");

                isInviteMode = false;
                if (loginListener != null)
                    loginListener.connexionSuccesful();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("sfss", "klkml");

                isInviteMode = true;
            }
        });
    }

    public void mdpOublier(String mail, final SendPasswordListener sendPasswordListener) {

        mAuth.sendPasswordResetEmail(mail).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (sendPasswordListener != null)
                    sendPasswordListener.onOperationComplete(task.isSuccessful());
            }
        });
    }

    public void deconnexion() {

        mAuth.signOut();

    }

    public void setInviteMode(boolean inviteMode) {
        isInviteMode = inviteMode;
    }

    public boolean isInviteMode() {
        return isInviteMode;
    }

    public UserItem getUser() {
        return user;
    }

    public static boolean isEmailValid(String email) {
        // return email.matches(EMAIL_PATTERN);

        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();

    }


    public interface LoginListener {
        void createAccountSuccesful();

        void createAccountFailed(Exception e);

        void connexionSuccesful();

        void connexionFailed(Exception e);
    }

    public interface SendPasswordListener {
        void onOperationComplete(boolean isSuccessful);
    }
}
