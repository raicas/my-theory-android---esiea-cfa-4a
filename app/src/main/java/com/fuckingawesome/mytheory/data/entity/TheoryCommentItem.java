package com.fuckingawesome.mytheory.data.entity;

import com.google.firebase.database.Exclude;

import java.util.HashMap;

import static com.google.firebase.database.ServerValue.TIMESTAMP;

/**
 * Created by allan on 02/04/2018.
 */

public class TheoryCommentItem {

    private HashMap<String, Object> timestampCreated;
    private String content;
    private String userId;
    private String userName;
    private String id;

    public TheoryCommentItem() {
        timestampCreated = new HashMap<>();
        this.timestampCreated.put("timestamp", TIMESTAMP);
    }

    public TheoryCommentItem(String content, String userId, String userName, String id) {
        this();
        this.content = content;
        this.userId = userId;
        this.userName = userName;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public HashMap<String, Object> getTimestampCreated() {
        return timestampCreated;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timestampCreated.get("timestamp");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
