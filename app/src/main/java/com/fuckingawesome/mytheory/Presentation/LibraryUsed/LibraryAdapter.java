package com.fuckingawesome.mytheory.Presentation.LibraryUsed;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fuckingawesome.mytheory.GlideUtil;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.LibraryItem;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by allan on 08/04/2018.
 */

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.ViewHolder> {

    private List<LibraryItem> libraries = new ArrayList<>();


    public LibraryAdapter() {


    }


    @Override
    public LibraryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.librairy_item, parent, false);

        final LibraryAdapter.ViewHolder holder = new LibraryAdapter.ViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int itemPosition = holder.getAdapterPosition();
                LibraryItem itm = libraries.get(itemPosition);
                String url = itm.getLink();
                if (url != null) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    view.getContext().startActivity(i);

                }

            }
        });
        return holder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(LibraryAdapter.ViewHolder holder, int position) {

        LibraryItem itm = libraries.get(position);
        Glide.with(holder.context).load(itm.getImg()).apply(GlideUtil.options).into(holder.libraryImageView);
        holder.titleLibraryTextView.setText(itm.getNom());
        holder.licenseLibrabryTextView.setText(itm.getLicense());


    }

    public void refreshLibrary(List<LibraryItem> libraries) {

        this.libraries.clear();

        this.libraries.addAll(libraries);

        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return libraries.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public Context context;
        public ImageView libraryImageView;
        public TextView titleLibraryTextView;
        public TextView licenseLibrabryTextView;

        public ViewHolder(View v) {
            super(v);

            libraryImageView = v.findViewById(R.id.libraryImage);
            titleLibraryTextView = v.findViewById(R.id.libraryTitleTextView);
            licenseLibrabryTextView = v.findViewById(R.id.libraryLicenseTextView);
            context = libraryImageView.getContext();
        }
    }


}
