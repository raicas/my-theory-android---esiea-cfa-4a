package com.fuckingawesome.mytheory.Presentation.Home;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.TheoryCommentItem;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by allan on 02/04/2018.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private List<TheoryCommentItem> comments;


    // Provide a suitable constructor (depends on the kind of dataset)
    public CommentAdapter(List<TheoryCommentItem> comments) {
        this.comments = comments;
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);

        final ViewHolder holder = new ViewHolder(view);


        return holder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        TheoryCommentItem itm = comments.get(position);
        holder.commentaireTextView.setText(itm.getContent());
        holder.commentaireUsernameTextView.setText(itm.getUserName());

    }

    @Override
    public int getItemCount() {
        Log.d("dsfsfsf", Integer.toString(comments.size()));
        return comments.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        public TextView commentaireTextView;
        public TextView commentaireUsernameTextView;

        public ViewHolder(View v) {
            super(v);
            commentaireTextView = v.findViewById(R.id.commentaireTextView);
            commentaireUsernameTextView = v.findViewById(R.id.commentaireUsernameTextView);
        }
    }

}
