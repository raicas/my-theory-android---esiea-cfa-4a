,----------------,              ,---------,
	        ,-----------------------,          ,"        ,"|
	      ,"                      ,"|        ,"        ,"  |
	     +-----------------------+  |      ,"        ,"    |
	     |  .-----------------.  |  |     +---------+      |
	     |  |  Bonjour !      |  |  |     | -==----'|      |
	     |  |  Bienvenue sur  |  |  |     |         |      |
	     |  |  le READ ME     |  |  |/----|`---=    |      |
	     |  |  du Projet      |  |  |   ,/|==== ooo |      ;
	     |  |  android 2018   |  |  |  // |(((( [33]|    ,"
	     |  `-----------------'  |," .;'| |((((     |  ,"
	     +-----------------------+  ;;  | |         |,"
	        /_)______________(_/  //'   | +---------+
	   __________________________/__  `,
	  /  oooooooooooooooo  .o.  oooo /,   \,"-----------
	 / ==ooooooooooooooo==.o.  ooo= //   ,`\--{)B     ,"
	/_==__==========__==_ooo__ooo=_/'   /___________,"
	`-----------------------------'

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GAU Julia
LO CASALE Lorenzo
LOUBER Allan
MALHERBE Cyril

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
L'apk du projet est disponible sur le git.

Notre projet:

Nous sommes partis d'un projet que nous avions r�alis� l'ann�e derni�re afin d'apprendre les enjeux du refactoring.

Ce projet utilise l'architecture MVVM (Model - View - ViewModel)

L"application � �t� localiser en deux langue, Fran�ais et Anglais, gr�ce au fichier strings.xml.

Notre projet comporte plusieurs activity (pages de l'application):
	- Une page de login
	- Une page d'accueil
	- Une page de d�tails des th�ories
Ainsi qu'une bottom navigation bar 

Afin d'avoir un code plus facilement maintenable, nous avons suivis le principe de clean architecture.
Cela nous permet d'avoir une s�paration des fonctions au niveau du code, une meilleur coh�rence
et �galement de rendre notre code plus facilement testable vu que la logique est s�par� de l'affichage.
La transition entre plusieurs activities est faite gr�ce au shared �l�ments et la Base de donn�es est aliment� par firebase.

Nous avons �galement un service qui utilise Retrofit et permet de r�cup�rer la liste des librairies utilis�es.

Les pr�f�rences de l'utilisateur sont conserv�es une fois l'application ferm�e gr�ce � l'utilisation des Shared Preferences.

L'application peut �tre utilis�e en mode paysage.


Malheureusement nous avons rencontr� un souci au niveau du gitflow car Android Studio fait du fast forward lors d'un merge, ce qui rend le graphisme du gitflow moins lisible mais le r�sultat reste le m�me.