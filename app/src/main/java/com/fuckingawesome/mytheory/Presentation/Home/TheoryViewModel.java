package com.fuckingawesome.mytheory.Presentation.Home;

import com.fuckingawesome.mytheory.data.FirebaseQueries;
import com.fuckingawesome.mytheory.data.IDataRepo;
import com.fuckingawesome.mytheory.data.Repository.FirebaseDataRepository;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TheoryViewModel extends ViewModel {


    DatabaseReference theoriesRef = FirebaseQueries.theoriesRef;
    private IDataRepo firebaseDataRepo;

    private MutableLiveData<List<TheoryItem>> theories = new MutableLiveData<List<TheoryItem>>();

    public TheoryViewModel() {
        firebaseDataRepo = new FirebaseDataRepository();
    }

    public LiveData<List<TheoryItem>> getTheories() {
        return firebaseDataRepo.getTheories();
    }

    public LiveData<List<TheoryItem>> getMyTheories() {
        return firebaseDataRepo.getMyTheories();
    }

}
