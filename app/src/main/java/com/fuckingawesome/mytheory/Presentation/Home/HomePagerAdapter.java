package com.fuckingawesome.mytheory.Presentation.Home;

import com.fuckingawesome.mytheory.R;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

/**
 * Created by allan on 08/04/2018.
 */

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private TheoryListFragment newestTheoryFragment = TheoryListFragment.newInstance(false);
    private TheoryListFragment myTheoryFragment = TheoryListFragment.newInstance(true);


    public HomePagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int i) {

        if (i == 0)
            return newestTheoryFragment;
        else
            return myTheoryFragment;


    }

    @Override
    public int getCount() {
        return 2;
    }


    public int getPageFragmentTitle(int position) {
        return position == 0 ? R.string.all_theories_page_title : R.string.my_theories_page_title;
    }

}
