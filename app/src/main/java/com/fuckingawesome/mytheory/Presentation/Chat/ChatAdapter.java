package com.fuckingawesome.mytheory.Presentation.Chat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fuckingawesome.mytheory.Presentation.Login.LoginHandler;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.ChatItem;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by allan on 02/04/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private List<ChatItem> messages = new ArrayList<>();


    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatAdapter() {

    }

    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View view;

        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_user_item, parent, false);

                break;

            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_opponent_item, parent, false);
                break;


        }

        ViewHolder holder = new ViewHolder(view);
        return holder;

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        ChatItem itm = messages.get(position);
        holder.messageTextView.setText(itm.getMessage());
        holder.userNameTextView.setText(itm.getUserName());
        //holder.dateTextView.setText(itm.getTitle());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView messageTextView;
        public TextView userNameTextView;
        public TextView dateTextView;

        public ViewHolder(View v) {
            super(v);
            messageTextView = v.findViewById(R.id.ChatMessageTextView);
            userNameTextView = v.findViewById(R.id.ChatUserNameTextView);
            dateTextView = v.findViewById(R.id.dateMessageTextView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        ChatItem itm = messages.get(position);
        Log.d("dikjlk", String.valueOf(!LoginHandler.instance.isInviteMode() && LoginHandler.instance.getUser().getIdUser().equals(itm.getIdUser())));
        return !LoginHandler.instance.isInviteMode() && LoginHandler.instance.getUser().getIdUser().equals(itm.getIdUser()) ? 0 : 1;
    }

    public void newMessageAdded(ChatItem chatItem) {
        messages.add(chatItem);
        notifyItemInserted(messages.size() - 1);
    }

}
