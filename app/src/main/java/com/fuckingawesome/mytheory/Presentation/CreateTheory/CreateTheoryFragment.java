package com.fuckingawesome.mytheory.Presentation.CreateTheory;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.fuckingawesome.mytheory.data.entity.UserItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CreateTheoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreateTheoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateTheoryFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private EditText titleTheoryEditText;
    private EditText contentTheoryEditText;
    private EditText couvertureImageEditText;
    private static final String IMAGE_PATTERN =
            "([^\\s]+(\\.(?i)(jpg|jpeg|png|gif|bmp))$)";
    private Pattern pattern = Pattern.compile(IMAGE_PATTERN);
    private Matcher matcher;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CreateTheoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateTheoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateTheoryFragment newInstance(String param1, String param2) {
        CreateTheoryFragment fragment = new CreateTheoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_theory, container, false);

        titleTheoryEditText = view.findViewById(R.id.title_theory_create);
        contentTheoryEditText = view.findViewById(R.id.content_theory_create);
        couvertureImageEditText = view.findViewById(R.id.couvertureEditText);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //    throw new RuntimeException(context.toString()
            //           + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public TheoryItem getNewTheory(String idTheory, UserItem user) {

        String titre = titleTheoryEditText.getText().toString().trim();
        String content = contentTheoryEditText.getText().toString().trim();
        String couvertureImage = couvertureImageEditText.getText().toString().trim();
        String idUser = "";
        String username = "";
        if (user != null) {
            idUser = user.getIdUser();
            username = user.getUserName();
        }
        TheoryItem theory = new TheoryItem(titre, content, idUser, idTheory, username, couvertureImage);


        return theory;
    }

    public void clearEditText() {
        titleTheoryEditText.getText().clear();
        contentTheoryEditText.getText().clear();
        couvertureImageEditText.getText().clear();
    }


    public boolean isPictureISValid(final String image) {

        matcher = pattern.matcher(image);
        return matcher.matches();

    }
}
