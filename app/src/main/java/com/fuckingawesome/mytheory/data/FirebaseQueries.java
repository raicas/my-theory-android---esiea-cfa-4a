package com.fuckingawesome.mytheory.data;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by allan on 13/03/2018.
 */

public class FirebaseQueries {

    public static final FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static final DatabaseReference theoriesRef = database.getReference("Theories");
    public static final DatabaseReference commentairesRef = database.getReference("Commentaires");
    public static final DatabaseReference usersRef = database.getReference("Users");
    public static final DatabaseReference chatRef = database.getReference("Chat");


}
