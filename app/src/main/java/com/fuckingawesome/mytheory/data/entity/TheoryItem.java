package com.fuckingawesome.mytheory.data.entity;

import com.google.firebase.database.Exclude;

import java.util.HashMap;

import static com.google.firebase.database.ServerValue.TIMESTAMP;

/**
 * Created by allan on 13/03/2018.
 */

public class TheoryItem {
    private HashMap<String, Object> timestampCreated;
    private String title;
    private String content;
    private String userId;
    private String id;
    private String userName;
    private String couvertureImage;

    public TheoryItem() {
        timestampCreated = new HashMap<>();
        this.timestampCreated.put("timestamp", TIMESTAMP);
    }

    public TheoryItem(String title, String content, String userId, String id, String userName, String couvertureImage) {
        this();
        this.title = title;
        this.content = content;
        this.userId = userId;
        this.id = id;
        this.userName = userName;
        this.couvertureImage = couvertureImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public HashMap<String, Object> getTimestampCreated() {
        return timestampCreated;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timestampCreated.get("timestamp");
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCouvertureImage() {
        return couvertureImage;
    }

    public void setCouvertureImage(String couvertureImage) {
        this.couvertureImage = couvertureImage;
    }
}
