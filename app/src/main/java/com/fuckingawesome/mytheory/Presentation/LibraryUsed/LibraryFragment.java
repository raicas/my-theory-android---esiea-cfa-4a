package com.fuckingawesome.mytheory.Presentation.LibraryUsed;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.fuckingawesome.mytheory.LibraryService;
import com.fuckingawesome.mytheory.ParamPreferences;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.LibraryItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static com.fuckingawesome.mytheory.LibraryService.LIBRARY_FILE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LibraryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LibraryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LibraryFragment extends DialogFragment {

    public static final String LIBRARY_UPDATE = "LIBRARY_UPDATE";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView mRecyclerView;
    private LibraryAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final String defaultChannel = "default";
    private static final String JSON_OBJECT_NAME = "libs";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public LibraryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LibraryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LibraryFragment newInstance(String param1, String param2) {
        LibraryFragment fragment = new LibraryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        LibraryService.startActionGET_LIBRARY(getContext());
        IntentFilter intentFilter = new IntentFilter(LIBRARY_UPDATE);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(new LibraryUpdate(), intentFilter);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vue = inflater.inflate(R.layout.fragment_library, container, false);

        mRecyclerView = vue.findViewById(R.id.libraryRecyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new LibraryAdapter();
        mRecyclerView.setAdapter(mAdapter);
        refreshLibrariesList();

        return vue;
    }

    @Override
    public void onResume() {
        super.onResume();


        Window window = getDialog().getWindow();
        Point size = new Point();
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        getDialog().getWindow().setLayout((int) (size.x * 0.75), (int) (size.y * 0.75));

        initChannels(getContext());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
//            throw new RuntimeException(context.toString()
            //         + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void refreshLibrariesList() {

        if (getActivity() == null)
            return;
        List<LibraryItem> libraries = new ArrayList<>();

        Iterator<String> iter = null;
        try {
            JSONObject jsonObject = getLibraryFromFile();
            jsonObject = jsonObject.getJSONObject(JSON_OBJECT_NAME);
            iter = jsonObject.keys();
            while (iter.hasNext()) {
                String key = iter.next();
                try {
                    LibraryItem libraryItem = LibraryItem.createLibraryItemFromJsonTheoryItem(jsonObject.getJSONObject(key));
                    libraries.add(libraryItem);
                } catch (JSONException e) {
                }
            }

            mAdapter.refreshLibrary(libraries);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private JSONObject getLibraryFromFile() {
        try {
            File file = new File(getContext().getCacheDir(), LIBRARY_FILE);
            InputStream is = new FileInputStream(file);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();


            return new JSONObject(new String(buffer, "UTF-8")); // construction du tableau
        } catch (IOException e) {
            e.printStackTrace();
            return new JSONObject();
        } catch (JSONException e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public class LibraryUpdate extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            // todo :  vérifier si c'est bien l'intent quon voulais

            if (context != null && ParamPreferences.instance.getNotificationParam(context)) {

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, defaultChannel)
                        .setSmallIcon(R.drawable.ic_update)
                        .setContentTitle(getString(R.string.lib_download_finished))
                        .setContentText(getString(R.string.lib_list_updated))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(getString(R.string.lib_list_updated)))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

                notificationManager.notify(5, mBuilder.build());
            }
            refreshLibrariesList();
        }


    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(defaultChannel,
                "Channel name",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }
}
