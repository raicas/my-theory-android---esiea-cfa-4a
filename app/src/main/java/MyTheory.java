import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by allan on 26/03/2018.
 */

public class MyTheory extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
