package com.fuckingawesome.mytheory.Presentation.Home;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TheoryListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TheoryListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TheoryListFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    private Boolean myTheoryAreNeeded = false;

    private OnFragmentInteractionListener mListener;

    private RecyclerView mRecyclerView;
    private TheoryListAdapter mAdapter;
    private GridLayoutManager mLayoutManager;

    private TheoryViewModel theoryViewModel;


    public TheoryListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    public static TheoryListFragment newInstance(Boolean param1) {
        TheoryListFragment fragment = new TheoryListFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, param1);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            myTheoryAreNeeded = getArguments().getBoolean(ARG_PARAM1);
        }
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_sort) {
            mAdapter.reverseList();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vue = inflater.inflate(R.layout.fragment_theory_list, container, false);

        mRecyclerView = vue.findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(getContext(), 2);


        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new TheoryListAdapter();


        mRecyclerView.setAdapter(mAdapter);

        theoryViewModel = new TheoryViewModel();

        // Create the observer which updates the UI.
        final Observer<List<TheoryItem>> theoriesObserver = new Observer<List<TheoryItem>>() {
            @Override
            public void onChanged(@Nullable final List<TheoryItem> theories) {
                // Update the UI, in this case
                mAdapter.refreshList(theories);
                Log.d("eeeeee", "théories reçççççuuuuu " + theories.size());
            }
        };

        if (myTheoryAreNeeded)
            theoryViewModel.getMyTheories().observe(this, theoriesObserver);
        else
            theoryViewModel.getTheories().observe(this, theoriesObserver);

        mLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position % 3 == 0 ? mLayoutManager.getSpanCount() : 1;
            }
        });

        return vue;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            //  throw new RuntimeException(context.toString()
            //         + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
