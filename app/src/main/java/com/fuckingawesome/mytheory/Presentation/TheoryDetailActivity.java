package com.fuckingawesome.mytheory.Presentation;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fuckingawesome.mytheory.GlideUtil;
import com.fuckingawesome.mytheory.Presentation.Home.CommentAdapter;
import com.fuckingawesome.mytheory.Presentation.Login.LoginHandler;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.FirebaseQueries;
import com.fuckingawesome.mytheory.data.entity.TheoryCommentItem;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class TheoryDetailActivity extends AppCompatActivity {

    public final static String THEORY_ITEM = "THEORY_ITEM";
    public DatabaseReference commentairesTheoryRef;
    private TheoryItem theoryItem;

    private ImageView couvertureTheoryItemDetailActivity;
    private TextView titreTheoryItemDetailActivity;
    private TextView contenuTheoryItemDetailActivity;
    private EditText commentaireEditText;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<TheoryCommentItem> comments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theory_detail);
        String data = getIntent().getStringExtra(THEORY_ITEM);
        theoryItem = new Gson().fromJson(data, TheoryItem.class);
        commentairesTheoryRef = FirebaseQueries.commentairesRef.child(theoryItem.getId());

        couvertureTheoryItemDetailActivity = findViewById(R.id.couvertureTheoryItemDetailActivity);
        titreTheoryItemDetailActivity = findViewById(R.id.titreTheoryItemDetailActivity);
        contenuTheoryItemDetailActivity = findViewById(R.id.contenuTheoryItemDetailActivity);
        commentaireEditText = findViewById(R.id.commentaireEditText);


        titreTheoryItemDetailActivity.setText(theoryItem.getTitle());


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            contenuTheoryItemDetailActivity.setText(Html.fromHtml(theoryItem.getContent(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            contenuTheoryItemDetailActivity.setText(Html.fromHtml(theoryItem.getContent()));
        }

        contenuTheoryItemDetailActivity.setMovementMethod(LinkMovementMethod.getInstance());

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            contenuTheoryItemDetailActivity.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);

        Glide.with(this).load(theoryItem.getCouvertureImage()).apply(GlideUtil.options).into(couvertureTheoryItemDetailActivity);


        mRecyclerView = findViewById(R.id.comment_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        mAdapter = new CommentAdapter(comments);
        mRecyclerView.setAdapter(mAdapter);
        refreshList();
    }

    public void onSendCommentClick(View view) {
        LoginHandler loginHandler = LoginHandler.instance;
        if (!loginHandler.isInviteMode()) {//vérifie si on est connecté
            String commentaire = commentaireEditText.getText().toString();
            String userId = loginHandler.getUser().getIdUser();
            String username = loginHandler.getUser().getUserName();
            //vérifier si le texte est bon
            DatabaseReference dbr = commentairesTheoryRef.push();
            TheoryCommentItem theoryCommentaire = new TheoryCommentItem(commentaire, userId, username, dbr.getKey());

            dbr.setValue(theoryCommentaire);
        }
        //Todo : mettre un toast
    }

    private void refreshList() {
        commentairesTheoryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments.clear();
                Log.d("dsfsfsf", "changed");
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.d("dsfsfsf", "data received");
                    comments.add(postSnapshot.getValue(TheoryCommentItem.class));
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("dsfsfsf", databaseError.getMessage());
            }
        });
    }
}
