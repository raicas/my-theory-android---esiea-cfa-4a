package com.fuckingawesome.mytheory.data.entity;

import com.google.firebase.database.Exclude;

import java.util.HashMap;

import static com.google.firebase.database.ServerValue.TIMESTAMP;

/**
 * Created by allan on 13/03/2018.
 */

public class ChatItem {

    private String idMessage;
    private String idUser;
    private String userName;
    private String message;
    private HashMap<String, Object> timestampCreated;


    public ChatItem() {
        timestampCreated = new HashMap<>();
        this.timestampCreated.put("timestamp", TIMESTAMP);
    }

    public ChatItem(String idMessage, String idUser, String userName, String message) {
        this();
        this.idMessage = idMessage;
        this.idUser = idUser;
        this.userName = userName;
        this.message = message;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Exclude
    public long getTimestampCreatedLong() {
        return (long) timestampCreated.get("timestamp");
    }

    public HashMap<String, Object> getTimestampCreated() {
        return timestampCreated;
    }

}
