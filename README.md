,----------------,              ,---------,
	        ,-----------------------,          ,"        ,"|
	      ,"                      ,"|        ,"        ,"  |
	     +-----------------------+  |      ,"        ,"    |
	     |  .-----------------.  |  |     +---------+      |
	     |  |  Bonjour !      |  |  |     | -==----'|      |
	     |  |  Bienvenue sur  |  |  |     |         |      |
	     |  |  le READ ME     |  |  |/----|`---=    |      |
	     |  |  du Projet      |  |  |   ,/|==== ooo |      ;
	     |  |  android 2018   |  |  |  // |(((( [33]|    ,"
	     |  `-----------------'  |," .;'| |((((     |  ,"
	     +-----------------------+  ;;  | |         |,"
	        /_)______________(_/  //'   | +---------+
	   __________________________/__  `,
	  /  oooooooooooooooo  .o.  oooo /,   \,"-----------
	 / ==ooooooooooooooo==.o.  ooo= //   ,`\--{)B     ,"
	/_==__==========__==_ooo__ooo=_/'   /___________,"
	`-----------------------------'

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GAU Julia
LO CASALE Lorenzo
LOUBER Allan
MALHERBE Cyril

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
L'apk du projet est disponible sur le git.

Notre projet:

Nous sommes partis d'un projet que nous avions réalisé l'année dernière afin d'apprendre les enjeux du refactoring.

Ce projet utilise l'architecture MVVM (Model - View - ViewModel)

L"application à été localiser en deux langue, Français et Anglais, grâce au fichier strings.xml.

Notre projet comporte plusieurs activity (pages de l'application):
	- Une page de login
	- Une page d'accueil
	- Une page de détails des théories
Ainsi qu'une bottom navigation bar 

Afin d'avoir un code plus facilement maintenable, nous avons suivis le principe de clean architecture.
Cela nous permet d'avoir une séparation des fonctions au niveau du code, une meilleur cohérence
et également de rendre notre code plus facilement testable vu que la logique est séparé de l'affichage.
La transition entre plusieurs activities est faite grâce au shared éléments et la Base de données est alimenté par firebase.

Nous avons également un service qui utilise Retrofit et permet de récupérer la liste des librairies utilisées.

Les préférences de l'utilisateur sont conservées une fois l'application fermée grâce à l'utilisation des Shared Preferences.

L'application peut être utilisée en mode paysage.


Malheureusement nous avons rencontré un souci au niveau du gitflow car Android Studio fait du fast forward lors d'un merge, ce qui rend le graphisme du gitflow moins lisible mais le résultat reste le même.