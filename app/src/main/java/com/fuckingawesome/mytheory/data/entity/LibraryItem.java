package com.fuckingawesome.mytheory.data.entity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by allan on 03/04/2018.
 */

public class LibraryItem implements Serializable {

    private static final String NOM = "nom";
    private static final String IMG = "img";
    private static final String LICENSE = "license";
    private static final String LINK = "link";


    private String nom;
    private String img;
    private String license;
    private String link;

    public LibraryItem(String nom, String img, String license, String link) {
        this.nom = nom;
        this.img = img;
        this.license = license;
        this.link = link;
    }

    public static LibraryItem createLibraryItemFromJsonTheoryItem(JSONObject jsonObject) {


        try {
            String nom = jsonObject.getString(NOM);
            String img = jsonObject.getString(IMG);
            String license = jsonObject.getString(LICENSE);
            String link = jsonObject.getString(LINK);

            return new LibraryItem(nom, img, license, link);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }

    public String getNom() {
        return nom;
    }


    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
