package com.fuckingawesome.mytheory.data.Repository;

import android.util.Log;

import com.fuckingawesome.mytheory.Presentation.Login.LoginHandler;
import com.fuckingawesome.mytheory.data.FirebaseQueries;
import com.fuckingawesome.mytheory.data.IDataRepo;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

/**
 * Created by allan on 13/11/2018.
 */

public class FirebaseDataRepository implements IDataRepo {

    //Faire un singleton

    DatabaseReference theoriesRef = FirebaseQueries.theoriesRef;

    private MutableLiveData<List<TheoryItem>> theories = new MutableLiveData<List<TheoryItem>>();
    private MutableLiveData<List<TheoryItem>> myTheories = new MutableLiveData<List<TheoryItem>>();



    public FirebaseDataRepository() {


    }

    public LiveData<List<TheoryItem>> getMyTheories() {


        if (!LoginHandler.instance.isInviteMode())
            theoriesRef.orderByChild("userId").equalTo(LoginHandler.instance.getUser().getIdUser()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    List<TheoryItem> theoriesTmp = new ArrayList<>();
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {


                        theoriesTmp.add(postSnapshot.getValue(TheoryItem.class));
                    }

                    Collections.reverse(theoriesTmp);

                    myTheories.postValue(theoriesTmp);

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.d("dsfsfsf", databaseError.getMessage());
                }
            });


        return myTheories;
    }


    public LiveData<List<TheoryItem>> getTheories() {

        theoriesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<TheoryItem> theoriesTmp = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    theoriesTmp.add(postSnapshot.getValue(TheoryItem.class));
                }

                Collections.reverse(theoriesTmp);

                theories.postValue(theoriesTmp);
                Log.d("eeeeee", "gzt théories reçççççuuuuu " + theoriesTmp.size());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("dsfsfsf", databaseError.getMessage());
            }
        });


        return theories;
    }


}
