package com.fuckingawesome.mytheory.Presentation;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.CheckBox;
import android.widget.Toast;

import com.fuckingawesome.mytheory.ParamPreferences;
import com.fuckingawesome.mytheory.Presentation.Chat.ChatFragment;
import com.fuckingawesome.mytheory.Presentation.CreateTheory.CreateTheoryFragment;
import com.fuckingawesome.mytheory.Presentation.Home.HomeFragment;
import com.fuckingawesome.mytheory.Presentation.LibraryUsed.LibraryFragment;
import com.fuckingawesome.mytheory.Presentation.Login.LoginActivity;
import com.fuckingawesome.mytheory.Presentation.Login.LoginHandler;
import com.fuckingawesome.mytheory.R;
import com.fuckingawesome.mytheory.data.FirebaseQueries;
import com.fuckingawesome.mytheory.data.entity.TheoryItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DatabaseReference;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity {


    private HomeFragment homeFragment = new HomeFragment();
    private CreateTheoryFragment createTheoryFragment = new CreateTheoryFragment();
    private SettingFragment settingFragment = new SettingFragment();
    private LibraryFragment libraryFragment = new LibraryFragment();
    private ChatFragment chatFragment = new ChatFragment();
    private static final int RESULT_CODE_INTRO_ACTIVITY = 21;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        replaceFragment(homeFragment, false);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_setting:
                                getSupportFragmentManager().popBackStack();
                                replaceFragment(settingFragment, true);
                                break;
                            case R.id.action_home:
                                if (!homeFragment.isVisible())
                                    getSupportFragmentManager().popBackStack();
                                break;
                            case R.id.action_chat:
                                getSupportFragmentManager().popBackStack();
                                replaceFragment(chatFragment, true);
                                break;
                            case R.id.action_create:
                                getSupportFragmentManager().popBackStack();
                                replaceFragment(createTheoryFragment, true);
                                break;
                        }
                        return true;
                    }
                });

        launchIntroActivity(false);

    }


    public void replaceFragment(Fragment fragment, Boolean isAddingToStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_container, fragment);
        if (isAddingToStack)
            fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void onAddTheoryClick(View view) {

        LoginHandler loginHandler = LoginHandler.instance;
        if (!loginHandler.isInviteMode()) {//vérifie si on est connecté


            DatabaseReference dbr = FirebaseQueries.theoriesRef.push();
            TheoryItem newTheory = createTheoryFragment.getNewTheory(dbr.getKey(), loginHandler.getUser());
            if (newTheory.getCouvertureImage().isEmpty() || newTheory.getContent().isEmpty() || newTheory.getTitle().isEmpty()) {
                Toast.makeText(this, R.string.error_text_needed, Toast.LENGTH_LONG).show();
                return;
            }
            if (!URLUtil.isValidUrl(newTheory.getCouvertureImage()) || !createTheoryFragment.isPictureISValid(newTheory.getCouvertureImage())) {
                Toast.makeText(this, R.string.cover_pic_not_correct, Toast.LENGTH_LONG).show();
                return;
            }
            dbr.setValue(newTheory);
            createTheoryFragment.clearEditText();

            Toast.makeText(this, R.string.theoty_create, Toast.LENGTH_LONG).show();

        } else
            Toast.makeText(this, R.string.error_connexion_needed, Toast.LENGTH_LONG).show();


    }


    public void onLibraryUsedClick(View view) {
        showLibraryDialogFragment();
    }

    private void showLibraryDialogFragment() {
        FragmentManager fm = getSupportFragmentManager();

        libraryFragment.show(fm, "fragment_edit_name");

    }

    public void onLaunchIntroActivityClick(View view) {
        launchIntroActivity(true);
    }


    public void onDecoClick(View view) {
        LoginHandler.instance.deconnexion();

        LoginActivity.launchLoginActivity(this);
        finish();
        Toast.makeText(this, R.string.Disconnect, Toast.LENGTH_LONG).show();

    }

    private void launchIntroActivity(boolean forceViewing) {

        if (!ParamPreferences.instance.getIntroParam(this) || forceViewing) {
            Intent myIntent = new Intent(this, MainIntroActivity.class);
            this.startActivityForResult(myIntent, RESULT_CODE_INTRO_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == RESULT_CODE_INTRO_ACTIVITY) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ParamPreferences.instance.setIntroParam(true, getBaseContext());
            }
        }
    }

    public void onSendMessageClick(View view) {

        if (!LoginHandler.instance.isInviteMode())
            chatFragment.sendChatMessage();
        else
            Toast.makeText(this, R.string.error_connexion_needed, Toast.LENGTH_LONG).show();

    }

    public void onPreviewClick(View view) {

        TheoryItem item = createTheoryFragment.getNewTheory("", LoginHandler.instance.getUser());
        if (item.getCouvertureImage().isEmpty() || item.getContent().isEmpty() || item.getTitle().isEmpty()) {
            Toast.makeText(this, R.string.error_text_needed, Toast.LENGTH_LONG).show();
            return;
        }
        String data = new Gson().toJson(item);
        Intent intent = new Intent(this, TheoryDetailActivity.class);
        intent.putExtra(TheoryDetailActivity.THEORY_ITEM, data);
        this.startActivity(intent);
    }

    public void onNotificationSettingChanged(View view) {
        CheckBox checkBox = (CheckBox) view;

        ParamPreferences.instance.setNotificationParam(checkBox.isChecked(), this);
        Toast.makeText(this, R.string.param_saved, Toast.LENGTH_LONG).show();
    }

    public void onRateAppClick(View view) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.rate_app_message)
                .setTitle(R.string.rate_app);


        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
