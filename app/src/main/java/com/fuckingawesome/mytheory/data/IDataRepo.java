package com.fuckingawesome.mytheory.data;

import com.fuckingawesome.mytheory.data.entity.TheoryItem;

import java.util.List;

import androidx.lifecycle.LiveData;

public interface IDataRepo {

    LiveData<List<TheoryItem>> getTheories();

    LiveData<List<TheoryItem>> getMyTheories();
}
