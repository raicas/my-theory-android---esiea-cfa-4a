package com.fuckingawesome.mytheory;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by allan on 09/04/2018.
 */

public class ParamPreferences {

    private static final String SHARED_PREF_FILE = "MyTheorySharedPref";
    private static final String IS_NOTIFICATION_ENABLED_PARAM = "MyTheorySharedPref";
    private static final String IS_INTRO_SEEN_PARAM = "MyTheorySharedPref";
    static public final ParamPreferences instance = new ParamPreferences();

    private ParamPreferences() {

    }

    public SharedPreferences getSharedPreferene(Context context) {
        return context.getSharedPreferences(SHARED_PREF_FILE, Context.MODE_PRIVATE);
    }

    public boolean getNotificationParam(Context context) {
        return getSharedPreferene(context).getBoolean(IS_NOTIFICATION_ENABLED_PARAM, true);
    }

    public void setNotificationParam(Boolean notificationParam, Context context) {

        getSharedPreferene(context).edit().putBoolean(IS_NOTIFICATION_ENABLED_PARAM, notificationParam).apply();
    }

    public boolean getIntroParam(Context context) {

        return getSharedPreferene(context).getBoolean(IS_INTRO_SEEN_PARAM, false);
    }

    public void setIntroParam(Boolean introParam, Context context) {

        getSharedPreferene(context).edit().putBoolean(IS_INTRO_SEEN_PARAM, introParam).apply();
    }

}
