package com.fuckingawesome.mytheory.Presentation;

import android.os.Bundle;

import com.fuckingawesome.mytheory.R;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

/**
 * Created by allan on 09/04/2018.
 */

public class MainIntroActivity extends IntroActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro_intro)
                .description(R.string.intro_intro_descrisption)
                .image(R.drawable.common_google_signin_btn_icon_light)
                .background(R.color.colorAccent)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(false)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro_functionnalities)
                .description(R.string.intro_functionnalities_description)
                .image(R.drawable.common_google_signin_btn_icon_light)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorAccent)
                .canGoBackward(false)
                .scrollable(false)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro_functionnalities_cahier)
                .description(R.string.intro_functionnalities_cahier_description)
                .image(R.drawable.common_google_signin_btn_icon_light)
                .background(R.color.colorAccent)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(true)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro_bonus)
                .description(R.string.intro_bonus_description)
                .image(R.drawable.common_google_signin_btn_icon_light)
                .background(R.color.colorPrimary)
                .backgroundDark(R.color.colorAccent)
                .scrollable(true)
                .build());


        addSlide(new SimpleSlide.Builder()
                .title(R.string.intro_end)
                .description(R.string.intro_end_dzscription)
                .image(R.drawable.common_google_signin_btn_icon_light)
                .background(R.color.colorAccent)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(true)
                .build());
    }
}
